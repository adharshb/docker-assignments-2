# STEPS TO FOLLOW 
-----------------------
  
  -   Build image from Dockerfile
  
                docker build . -t adharsh-image(any-name)
  
  -   Create a custom network
  
                docker network create adharsh-nett(any-name)
  
  -   Run a php Container
  
                docker run -d --name adharsh-php -p 80:80 --network=adharsh-net adharsh-image
  
  -   Run a mysql container
  
                docker run --name adharsh-mysql --network=adharsh-net -p 3306:3306 -e MYSQL_ROOT_PASSWORD=check@1234 -d mysql:latest 
