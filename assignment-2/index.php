<h1>Hello Cloudreach!</h1>
<h4>Attempting MySQL connection from php...</h4>
<?php

/*
Functions get the data from secrets file
*/ 
function getSecret($secret){
    $secret_file = fopen("/run/secrets/{$secret}","r");
    $secret = fgets($secret_file);
    fclose($secret_file);
    
    return  $secret;                      
}

$host = 'db_t';
$user = 'root';
$pass = getSecret("mydb-password"); // get data from secrets

$conn = new mysqli($host, $user, $pass); // set-up mysql client

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

echo "Connected to MySQL successfully!";
?>
